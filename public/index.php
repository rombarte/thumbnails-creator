<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Factory\AppFactory;
use ThumbnailsCreator\Controller\ErrorController;
use ThumbnailsCreator\Controller\UploadController;
use ThumbnailsCreator\Core\Configuration;

require __DIR__ . '/../vendor/autoload.php';

try {
    $configuration = new Configuration(__DIR__ . '/../configuration.yaml');

    $app = AppFactory::create();

    $app->post(
        '/v1/thumbnails/',
        fn(ServerRequestInterface $request, ResponseInterface $response) => (new UploadController($configuration))->createFromImage($request, $response)
    );

    // Use default route to handle 404 error
    $app->any(
        '{route:.*}',
        fn(ServerRequestInterface $request, ResponseInterface $response) => (new ErrorController($configuration))->handleNotFound($request, $response)
    );

    $app->run();
} catch (Throwable $throwable) {
    http_response_code(500);

    echo json_encode(['status' => 'error', 'message' => 'Server is burning. Try again for few minutes.']);
}
