<?php

declare(strict_types=1);

namespace Tests\Image;

use PHPUnit\Framework\TestCase;
use ThumbnailsCreator\Image\DimensionCalculator;

class DimensionCalculatorTest extends TestCase
{
    /**
     * @dataProvider smallerDimensionsDataProvider
     */
    public function testWidthAndHeightIsBiggerThanMaxWidthAndShouldReturnIncreasedDimensions(array $testCase, array $expectedResult): void
    {
        $service = new DimensionCalculator();

        $result = $service->calcByLongerSideMaxWidth(...$testCase);

        $this->assertEquals($expectedResult, $result);
    }

    public function smallerDimensionsDataProvider(): array
    {
        return [
            [[150, 250, 100], [150, 60]],
            [[150, 118, 300], [59, 150]],
            [[123, 1000, 21], [122, 2]],
            [[998, 999, 997], [998, 996]],
        ];
    }

    public function testWidthAndHeightIsSmallerThanMaxWidthAndShouldReturnTheSameDimensions(): void
    {
        $service = new DimensionCalculator();

        $result = $service->calcByLongerSideMaxWidth(150, 120, 70);

        $this->assertEquals([120, 70], $result);
    }

    public function testMaxWidthEqualsZeroShouldThrowsException(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $service = new DimensionCalculator();

        $service->calcByLongerSideMaxWidth(0, 120, 70);
    }
}