<?php

declare(strict_types=1);

namespace ThumbnailsCreator;

interface RetrievableInterface
{
    /**
     * @return mixed|null
     */
    public function getValue(string $key);
}
