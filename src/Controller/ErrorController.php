<?php

declare(strict_types=1);

namespace ThumbnailsCreator\Controller;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ErrorController extends AbstractBaseController
{
    public function handleNotFound(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $responseData = ['status' => 'error'];

        $response = $response->withStatus(404);

        return $this->getResponseWithData($response, $responseData);
    }
}
