<?php

declare(strict_types=1);

namespace ThumbnailsCreator\Controller;

use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UploadedFileInterface;
use ThumbnailsCreator\Exception\SaveFileException;
use ThumbnailsCreator\Image\DimensionCalculator;
use ThumbnailsCreator\Image\ThumbnailsCreator;
use ThumbnailsCreator\Storage\StorageService;

class UploadController extends AbstractBaseController
{
    /**
     * @throws SaveFileException
     */
    public function createFromImage(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        /** @var UploadedFileInterface $uploadedImage */
        $uploadedImage = $request->getUploadedFiles()['image'] ?? null;
        $longerSideMaxWidth = $this->configuration->getValue('thumbnails.longerSideMaxWidth');
        $extension = $this->configuration->getValue('thumbnails.extension');

        if (!$uploadedImage || !$longerSideMaxWidth || !$extension) {
            throw new InvalidArgumentException('Mandatory field is not provided.');
        }

        if ($uploadedImage->getClientMediaType() !== 'image/png') {
            throw new InvalidArgumentException('File type not allowed.');
        }

        $thumbnailsCreator = new ThumbnailsCreator($longerSideMaxWidth, $extension);
        $thumbnailsCreator->setDimensionCalculator(new DimensionCalculator());
        $stream = $thumbnailsCreator->createFromStream($uploadedImage->getStream());

        $filename = $uploadedImage->getClientFilename();
        $storage = StorageService::createByConfiguration($this->configuration);
        $storage->save($filename, $stream);

        $responseData = ['status' => 'ok'];
        $response = $response->withStatus(201);
        return $this->getResponseWithData($response, $responseData);
    }
}
