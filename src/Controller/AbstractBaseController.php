<?php

declare(strict_types=1);

namespace ThumbnailsCreator\Controller;

use Psr\Http\Message\ResponseInterface;
use ThumbnailsCreator\RetrievableInterface;

abstract class AbstractBaseController
{
    protected RetrievableInterface $configuration;

    public function __construct(RetrievableInterface $configuration)
    {
        $this->configuration = $configuration;
    }

    protected function getResponseWithData(ResponseInterface $response, array $data): ResponseInterface
    {
        $responseData = json_encode($data);

        $response->getBody()->write($responseData);

        return $response;
    }
}
