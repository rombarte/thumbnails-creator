<?php

declare(strict_types=1);

namespace ThumbnailsCreator\Core;

use RuntimeException;
use Spyc;
use ThumbnailsCreator\RetrievableInterface;

class Configuration implements RetrievableInterface
{
    private array $config = [];

    public function __construct(string $pathToConfigFile)
    {
        if (!file_exists($pathToConfigFile)) {
            throw new RuntimeException('Configuration file doesn\'t exists');
        }

        $this->config = Spyc::YAMLLoad($pathToConfigFile);
    }

    /**
     * {@inheritDoc}
     */
    public function getValue(string $key)
    {
        return $this->config[$key] ?? null;
    }
}
