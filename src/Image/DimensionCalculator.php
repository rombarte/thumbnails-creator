<?php

declare(strict_types=1);

namespace ThumbnailsCreator\Image;

class DimensionCalculator
{
    public function calcByLongerSideMaxWidth(int $longerSideMaxWidth, int $width, int $height): array
    {
        if ($longerSideMaxWidth === 0) {
            throw new \InvalidArgumentException('Longer side max width can\'t be zero');
        }

        $longerSideWidth = $width > $height ? $width : $height;

        if ($longerSideWidth < $longerSideMaxWidth) {
            return [$width, $height];
        }

        $ratio = $longerSideWidth / $longerSideMaxWidth;

        return [(int) ($width / $ratio), (int) ($height / $ratio)];
    }
}
