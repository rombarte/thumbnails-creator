<?php

declare(strict_types=1);

namespace ThumbnailsCreator\Image;

use Intervention\Image\ImageManagerStatic as Image;
use Psr\Http\Message\StreamInterface;

class ThumbnailsCreator
{
    /**
     * @var int In pixels
     */
    private int $longerSideMaxWidth;

    /**
     * @var string Without dot
     */
    private string $fileExtension;

    private DimensionCalculator $dimensionCalculator;

    public function setDimensionCalculator(DimensionCalculator $dimensionCalculator): self
    {
        $this->dimensionCalculator = $dimensionCalculator;

        return $this;
    }

    public function __construct(int $longerSideMaxWidth, string $fileExtension)
    {
        $this->longerSideMaxWidth = $longerSideMaxWidth;
        $this->fileExtension = $fileExtension;
    }

    public function createFromStream(StreamInterface $image): StreamInterface
    {
        [$width, $height] = getimagesize($image->getMetadata('uri'));

        [$width, $height] = $this->dimensionCalculator->calcByLongerSideMaxWidth($this->longerSideMaxWidth, $width, $height);

        $image = Image::make($image)->resize($width, $height);

        return $image->stream($this->fileExtension);
    }
}
