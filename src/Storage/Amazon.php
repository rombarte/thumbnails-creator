<?php

declare(strict_types=1);

namespace ThumbnailsCreator\Storage;

use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Psr\Http\Message\StreamInterface;
use ThumbnailsCreator\Exception\SaveFileException;
use ThumbnailsCreator\RetrievableInterface;
use ThumbnailsCreator\StorableInterface;

class Amazon implements StorableInterface
{
    private RetrievableInterface $configuration;
    private S3Client $client;

    public function __construct(RetrievableInterface $configuration)
    {
        $this->configuration = $configuration;

        $this->client = new S3Client(
            [
                'version' => 'latest',
                'region' => $this->configuration->getValue('provider.aws.region') ?? '',
                'credentials' => [
                    'key' => $this->configuration->getValue('provider.aws.access_key') ?? '',
                    'secret' => $this->configuration->getValue('provider.aws.client_secret') ?? '',
                ],
            ]
        );
    }

    public function save(string $filename, StreamInterface $data): void
    {
        try {
            $path = $this->configuration->getValue('provider.aws.directory') ?? '';

            $this->client->putObject(
                [
                    'Bucket' => $this->configuration->getValue('provider.aws.bucket') ?? '',
                    'Key' => $path . '/' . $filename,
                    'Body' => $data->getContents(),
                    'ACL' => 'public-read',
                ]
            );
        } catch (S3Exception $e) {
            throw new SaveFileException('Can\'t write file.');
        }
    }
}
