<?php

declare(strict_types=1);

namespace ThumbnailsCreator\Storage;

use Kunnu\Dropbox\Dropbox as DropboxWrapper;
use Kunnu\Dropbox\DropboxApp;
use Psr\Http\Message\StreamInterface;
use ThumbnailsCreator\Exception\SaveFileException;
use ThumbnailsCreator\RetrievableInterface;
use ThumbnailsCreator\StorableInterface;

class Dropbox implements StorableInterface
{
    private RetrievableInterface $configuration;
    private DropboxWrapper $dropbox;

    public function __construct(RetrievableInterface $configuration)
    {
        $this->configuration = $configuration;

        $app = new DropboxApp(
            $this->configuration->getValue('provider.dropbox.client_id') ?? '',
            $this->configuration->getValue('provider.dropbox.client_secret') ?? '',
        );

        $this->dropbox = new DropboxWrapper($app);
    }

    public function save(string $filename, StreamInterface $data): void
    {
        $path = $this->configuration->getValue('provider.dropbox.directory') ?? '';

        if (!$this->dropbox->upload($path, $filename, ['autorename' => true])) {
            throw new SaveFileException('Can\'t write file.');
        }
    }
}
