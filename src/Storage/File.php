<?php

declare(strict_types=1);

namespace ThumbnailsCreator\Storage;

use Psr\Http\Message\StreamInterface;
use ThumbnailsCreator\Exception\SaveFileException;
use ThumbnailsCreator\RetrievableInterface;
use ThumbnailsCreator\StorableInterface;

class File implements StorableInterface
{
    private RetrievableInterface $configuration;

    public function __construct(RetrievableInterface $configuration)
    {
        $this->configuration = $configuration;
    }

    public function save(string $filename, StreamInterface $data): void
    {
        $path = $this->configuration->getValue('provider.file.directory') ?? '';

        if (!is_writable($path)) {
            throw new SaveFileException('Can\'t write file. Permission denied.');
        }

        if (!file_put_contents($path . DIRECTORY_SEPARATOR . $filename, $data)) {
            throw new SaveFileException('Can\'t write file.');
        }
    }
}
