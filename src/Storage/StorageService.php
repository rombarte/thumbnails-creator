<?php

declare(strict_types=1);

namespace ThumbnailsCreator\Storage;

use UnexpectedValueException;
use ThumbnailsCreator\RetrievableInterface;
use ThumbnailsCreator\StorableInterface;

class StorageService
{
    public const AWS_PROVIDER = 'aws';
    public const DROPBOX_PROVIDER = 'dropbox';
    public const FILE_PROVIDER = 'file';

    /**
     * @throws UnexpectedValueException
     */
    public static function createByConfiguration(RetrievableInterface $configuration): StorableInterface
    {
        switch ($configuration->getValue('thumbnails.provider') ?? '') {
            case self::AWS_PROVIDER:
                return new Amazon($configuration);
            case self::DROPBOX_PROVIDER:
                return new Dropbox($configuration);
            case self::FILE_PROVIDER:
                return new File($configuration);
            default:
                throw new UnexpectedValueException('Service provider must be chosen.');
        }
    }
}
