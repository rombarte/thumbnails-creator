<?php

declare(strict_types=1);

namespace ThumbnailsCreator;

use Psr\Http\Message\StreamInterface;
use ThumbnailsCreator\Exception\SaveFileException;

interface StorableInterface
{
    /**
     * @throws SaveFileException
     */
    public function save(string $filename, StreamInterface $data): void;
}
