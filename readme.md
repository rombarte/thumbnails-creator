![](https://gitlab.com/rombarte/thumbnails-creator/badges/master/pipeline.svg) 

# Thumbnails Creator
## Create and store your thumbnails fast!

Thumbnails Creator is a microservice to make thumbnails from images and upload it to configured by user service provider. At the moment supported providers are: `file storage`, `Amazon S3` and `Dropbox`.

Project is based on PHP 7.4 and Slim Framework 4.5.0.

### How to start

The easiest option to start working with application is using Docker with compose:

```shell script
docker-compose up -d
docker exec -it pl.rombarte.tc composer install --no-dev
```

Before start using application you have to prepare a configuration. Copy `configuration.yaml.dist` file to `configuration.yaml`. Remember - for security reasons - don't store configuration in your repository.

### Configuration options

Configuration is stored in application main directory in `configuration.yaml` file.

|Option|Type|Mandatory|Description|
|------|----|---------|-----------|
|thumbnails.provider|string|true|Service provider to store generated thumbnails. Allowed values are: **file**, **aws** or **dropbox**.
|thumbnails.longerSideMaxWidth|int|true|Thumbnail longer side maximum width (in pixels). Image would be resized to this value (if is bigger than limit).
|thumbnails.extension|string|true|Thumbnail type file. Thumbnails type would be converted to this value. Allowed value is **png**.
|provider.file.directory|string|false|Path to directory, where would be stored files. **Required, when using files provider**.
|provider.aws.access_key|string|false|Access key for AWS API. **Required, when using AWS provider**.
|provider.aws.client_secret|string|false|Secret token for AWS API. **Required, when using AWS provider**.
|provider.aws.region|string|false|AWS region code. **Required, when using Amazon provider**.
|provider.aws.bucket|string|false|AWS bucket name. **Required, when using Amazon provider**.
|provider.aws.directory|string|false|Path to directory, where would be stored files. **Required, when using Amazon provider**.
|provider.dropbox.client_id|string|false|Client ID for Dropbox API. **Required, when using Dropbox provider**.
|provider.dropbox.client_secret|string|false|Secret token for Dropbox API. **Required, when using Dropbox provider**.
|provider.dropbox.directory|string|false|Path to directory, where would be stored files. **Required, when using Dropbox provider**.

### API endpoints

Default domain is `http://localhost` when using Docker.

1. Upload thumbnail
    - **Endpoint**: `/v1/thumbnails`
    - **Method**: `POST`
    - **Content Type**: `multipart/form-data`
    - **Request parameters:**: 
        - **image**: `source image file for thumbnail. PNG file is only allowed.`
    - **Response**: 
        ```json
        { status: "<string>" }
      ```
      - **status**: `ok` when thumbnail has been created; `error` for failure

### Management and logging

1. Apache logs are available in `.docker/logs/access.log` and `.docker/logs/error.log` files.
2. VHost configuration is available in `.docker/000-default.conf` file.
3. PHP configuration is available in `.docker/php.ini`.

### Development
1. PHP has integration with Xdebug debugger. Configuration has been prepared to work with Docker. Use default `target` to start developing.

### TODO

1. More unit tests.
2. Local environment for testing AWS / Dropbox providers.
3. API's authentication.
4. Filter uploaded image name nad types for security reasons.
5. Return new resource when respond 201 status.
6. Thrown exception recording.
7. Resolve bug with JPG / BMP images.

### License

MIT License. More info about this license is [here](https://en.wikipedia.org/wiki/MIT_License).